#!/usr/bin/perl

# This script monitors a directoty for changes.
# Author: Tiago Melo (tiagoharris@gmail.com)

use common::sense;
use AnyEvent;
use AnyEvent::Loop;
use AnyEvent::Filesys::Notify;
use Cwd 'abs_path';
use Config::INI::Reader;
use sigtrap qw/die normal-signals/;

my $config    	= Config::INI::Reader->read_file('../conf/configuration.ini');
my $watch_dir 	= abs_path $config->{general}->{watch_dir}; 

# this is the function that will process the notifications.
sub process {
	my @notifications = @{$_[0]};
	
	foreach my $notification (@notifications) {
		# if we wanted to inspect the $notification object, we could do:
		#
		# use Data::Dumper;
		# say Dumper $file;
		# 
		# the output would be something like this:
		#{
        # 'path' => '/home/tiago/desenv/perl/tutorial/watched_dir/files/image.png',
        # 'type' => 'created',
        # 'is_dir' => 0
       	#}
       	#
       	# if the $notification object is related to a directory, the output would be something like this:
		#
		#{
        # 'path' => '/home/tiago/desenv/perl/tutorial/watched_dir/files',
        # 'type' => 'created',
        # 'is_dir' => 1
       	#}
       	# where 'type' can be 'created', 'modified' or 'deleted'.

       	my $file_type = $notification->is_dir ? "directory" : "regular file";

		say $notification->path . " was " . $notification->type . " -> $file_type";
  	}
}

# here we setup the notifier, specifying 'process' function as the callback.
# we pass to 'process' function a reference of an array of notifications.
my $notifier = AnyEvent::Filesys::Notify->new(
	dirs => [ $watch_dir ],
	cb   => sub {
		process \@_;
	},
	
	# http://search.cpan.org/~mgrimes/AnyEvent-Filesys-Notify-1.14/lib/AnyEvent/Filesys/Notify.pm
	#In backends that support it (currently INotify2), parse the events instead of rescanning file system for changed stat() information. 
	#Note, that this might cause slight changes in behavior. 
	#In particular, the Inotify2 backend will generate an additional 'modified' event 
	#when a file changes (once when opened for write, and once when modified).

	parse_events => 0,
);

# Event loop. This script will run until it is interrupted.
AnyEvent::Loop::run;