# directory-watcher

This is the source to ilustrate how to write a directory watcher in Perl.

## Stack:
* [AnyEvent](https://metacpan.org/pod/AnyEvent)
* [AnyEvent::Loop](https://metacpan.org/pod/AnyEvent::Loop)
* [AnyEvent::Filesys::Notify](https://metacpan.org/pod/AnyEvent::Filesys::Notify)
* [Config::INI::Reader](https://metacpan.org/pod/Config::INI::Reader)


